// SPDX-License-Identifier: GPL-2.0
/*
 * dbgp.c -- EHCI Debug Port device gadget
 *
 * Copyright (C) 2010 Stephane Duverger
 *
 * Released under the GPLv2.
 */

/* verbose messages */
#define DEBUG

#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/module.h>
#include <linux/usb/ch9.h>
#include <linux/usb/gadget.h>

#include "u_serial.h"

#define DRIVER_VENDOR_ID	0x0525 /* NetChip */
#define DRIVER_PRODUCT_ID	0x127a /* net20dc */

#define USB_DEBUG_MAX_PACKET_SIZE     512 // TODO: Make sure we can handle it once it happens with full ehci stack
#define DBGP_REQ_EP0_LEN              128
#define DBGP_REQ_LEN                  512

#define DBGP_CONFIG_ID			1
#define DBGP_CONFIG_STR_ID		4
#define DBGP_MANUFACTURER_STR_ID	1
#define DBGP_PRODUCT_STR_ID		2
#define DBGP_SERIAL_STR_ID		3
#define DBGP_BULK_CONFIG_STR_ID		4
#define DBGP_CONTROL_STR_ID		6
#define DBGP_DATA_STR_ID		7
#define DBGP_SHORT_NAME "g_dbgp"
#define DBGP_LONG_NAME "Ajays USB 2.0 Debug Cable"
#define DBGP_INTERFACE_ID		0
#define DBGP_MAX_DESC_LEN		256


static struct dbgp {
	struct usb_gadget  *gadget;
	struct usb_request *req;
	struct usb_ep      *i_ep;
	struct usb_ep      *o_ep;
#ifdef CONFIG_USB_G_DBGP_SERIAL
	struct gserial     *serial;
#endif
} dbgp;

static struct usb_string dbgp_strings[] = {
	{ DBGP_MANUFACTURER_STR_ID, "Ajays Technology" },
	{ DBGP_PRODUCT_STR_ID, DBGP_LONG_NAME },
	{ DBGP_SERIAL_STR_ID, "Ajays001" },
	{  } /* end of list */
};

static struct usb_gadget_strings dbgp_string_table = {
	.language =		0x0409,	/* en-us */
	.strings =		dbgp_strings,
};

static struct usb_device_descriptor device_desc = { // TODO: fix name
	.bLength = sizeof device_desc,
	.bDescriptorType = USB_DT_DEVICE,
	.bcdUSB = cpu_to_le16(0x0200),
	.bcdDevice = cpu_to_le16(0x0101),
	.bDeviceClass =	USB_CLASS_VENDOR_SPEC,
	.idVendor = cpu_to_le16(DRIVER_VENDOR_ID),
	.idProduct = cpu_to_le16(DRIVER_PRODUCT_ID),
	.iManufacturer = DBGP_MANUFACTURER_STR_ID,
	.iProduct = DBGP_PRODUCT_STR_ID,
	.iSerialNumber = DBGP_SERIAL_STR_ID,
	.bNumConfigurations = 1,
};

static struct usb_debug_descriptor dbg_desc = {
	.bLength = sizeof dbg_desc,
	.bDescriptorType = USB_DT_DEBUG,
};

static struct usb_endpoint_descriptor i_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bmAttributes = USB_ENDPOINT_XFER_BULK,
	.bEndpointAddress = USB_DIR_IN,
};

static struct usb_endpoint_descriptor o_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bmAttributes = USB_ENDPOINT_XFER_BULK,
	.bEndpointAddress = USB_DIR_OUT,
};

static struct usb_config_descriptor dbgp_config_desc = {
	.bLength =		USB_DT_CONFIG_SIZE,
	.bDescriptorType =	USB_DT_CONFIG,
	/* .wTotalLength computed dynamically */
	.bNumInterfaces =	1,
	.bConfigurationValue =	DBGP_CONFIG_ID,
	//.iConfiguration =	DBGP_CONFIG_STR_ID,
	.bmAttributes =		USB_CONFIG_ATT_ONE | USB_CONFIG_ATT_SELFPOWER,
	.bMaxPower =		0,
};

static const struct usb_interface_descriptor dbgp_interface_desc = {
	.bLength =		USB_DT_INTERFACE_SIZE,
	.bDescriptorType =	USB_DT_INTERFACE,
	.bInterfaceNumber =	DBGP_INTERFACE_ID,
	.bNumEndpoints =	2,
	.bInterfaceClass =	USB_CLASS_VENDOR_SPEC,
	.bInterfaceSubClass =	0,
	.bInterfaceProtocol =	0,
};

static const struct usb_descriptor_header *dbgp_highspeed_function[] = {
	//(struct usb_descriptor_header *) &dbgp_otg_descriptor,
	(struct usb_descriptor_header *) &dbgp_interface_desc,
	(struct usb_descriptor_header *) &i_desc, //TODO: fix name
	(struct usb_descriptor_header *) &o_desc, //TODO: fix name
	NULL,
};

static int dbgp_build_config_buf(u8 *buf, struct usb_gadget *g,
		u8 type, unsigned int index, int is_otg);

#ifdef CONFIG_USB_G_DBGP_PRINTK
static int dbgp_consume(char *buf, unsigned len)
{
	char c;

	if (!len)
		return 0;

	c = buf[len-1];
	if (c != 0)
		buf[len-1] = 0;

	printk(KERN_NOTICE "%s%c", buf, c);
	return 0;
}

static void __disable_ep(struct usb_ep *ep)
{
	usb_ep_disable(ep);
}

static void dbgp_disable_ep(void)
{
	__disable_ep(dbgp.i_ep);
	__disable_ep(dbgp.o_ep);
}

static void dbgp_complete(struct usb_ep *ep, struct usb_request *req)
{
	int stp;
	int err = 0;
	int status = req->status;

	if (ep == dbgp.i_ep) {
		stp = 1;
		goto fail;
	}

	if (status != 0) {
		stp = 2;
		goto release_req;
	}

	dbgp_consume(req->buf, req->actual);

	req->length = DBGP_REQ_LEN;
	err = usb_ep_queue(ep, req, GFP_ATOMIC);
	if (err < 0) {
		stp = 3;
		goto release_req;
	}

	return;

release_req:
	kfree(req->buf);
	usb_ep_free_request(dbgp.o_ep, req);
	dbgp_disable_ep();
fail:
	dev_dbg(&dbgp.gadget->dev,
		"complete: failure (%d:%d) ==> %d\n", stp, err, status);
}

static int dbgp_enable_ep_req(struct usb_ep *ep)
{
	int err, stp;
	struct usb_request *req;

	req = usb_ep_alloc_request(ep, GFP_KERNEL);
	if (!req) {
		err = -ENOMEM;
		stp = 1;
		goto fail_1;
	}

	req->buf = kzalloc(DBGP_REQ_LEN, GFP_KERNEL);
	if (!req->buf) {
		err = -ENOMEM;
		stp = 2;
		goto fail_2;
	}

	req->complete = dbgp_complete;
	req->length = DBGP_REQ_LEN;
	err = usb_ep_queue(ep, req, GFP_ATOMIC);
	if (err < 0) {
		stp = 3;
		goto fail_3;
	}

	return 0;

fail_3:
	kfree(req->buf);
fail_2:
	usb_ep_free_request(dbgp.o_ep, req);
fail_1:
	dev_dbg(&dbgp.gadget->dev,
		"enable ep req: failure (%d:%d)\n", stp, err);
	return err;
}

static int __enable_ep(struct usb_ep *ep, struct usb_endpoint_descriptor *desc)
{
	int err;
	ep->desc = desc;
	err = usb_ep_enable(ep);
	return err;
}

static int dbgp_enable_ep(void)
{
	int err, stp;

	err = __enable_ep(dbgp.i_ep, &i_desc);
	if (err < 0) {
		stp = 1;
		goto fail_1;
	}

	err = __enable_ep(dbgp.o_ep, &o_desc);
	if (err < 0) {
		stp = 2;
		goto fail_2;
	}

	err = dbgp_enable_ep_req(dbgp.o_ep);
	if (err < 0) {
		stp = 3;
		goto fail_3;
	}

	return 0;

fail_3:
	__disable_ep(dbgp.o_ep);
fail_2:
	__disable_ep(dbgp.i_ep);
fail_1:
	dev_dbg(&dbgp.gadget->dev, "enable ep: failure (%d:%d)\n", stp, err);
	return err;
}
#endif

static void dbgp_disconnect(struct usb_gadget *gadget)
{
#ifdef CONFIG_USB_G_DBGP_PRINTK
	dbgp_disable_ep();
#else
	dev_dbg(&dbgp.gadget->dev, "disconnected\n");
	gserial_disconnect(dbgp.serial);
#endif
}

/*
 * dbgp_build_config_buf
 *
 * Builds the config descriptors in the given buffer and returns the
 * length, or a negative error number.
 */
static int dbgp_build_config_buf(u8 *buf, struct usb_gadget *g,
	u8 type, unsigned int index, int is_otg)
{
	int len;
	//int high_speed = 0;
	const struct usb_config_descriptor *config_desc;
	const struct usb_descriptor_header **function;

	if (index >= device_desc.bNumConfigurations)
		return -EINVAL;

	/* other speed switches high and full speed */
	//if (gadget_is_dualspeed(g)) {
	//	high_speed = (g->speed == USB_SPEED_HIGH);
	//	if (type == USB_DT_OTHER_SPEED_CONFIG)
	//		high_speed = !high_speed;
	//}

	config_desc = &dbgp_config_desc;
	function = dbgp_highspeed_function;

	/* for now, don't advertise srp-only devices */
	//if (!is_otg)
	//	function++;

	len = usb_gadget_config_buf(config_desc, buf, DBGP_MAX_DESC_LEN, function);
	if (len < 0)
		return len;

	((struct usb_config_descriptor *)buf)->bDescriptorType = type;

	return len;
}

static void dbgp_unbind(struct usb_gadget *gadget)
{
#ifdef CONFIG_USB_G_DBGP_SERIAL
	kfree(dbgp.serial);
	dbgp.serial = NULL;
#endif
	if (dbgp.req) {
		kfree(dbgp.req->buf);
		usb_ep_free_request(gadget->ep0, dbgp.req);
		dbgp.req = NULL;
	}
}

#ifdef CONFIG_USB_G_DBGP_SERIAL
static unsigned char tty_line;
#endif

static int dbgp_configure_endpoints(struct usb_gadget *gadget)
{
	int stp;

	usb_ep_autoconfig_reset(gadget);

	dbgp.i_ep = usb_ep_autoconfig(gadget, &i_desc);
	if (!dbgp.i_ep) {
		stp = 1;
		goto fail_1;
	}

	i_desc.wMaxPacketSize =
		cpu_to_le16(USB_DEBUG_MAX_PACKET_SIZE);

	dbgp.o_ep = usb_ep_autoconfig(gadget, &o_desc);
	if (!dbgp.o_ep) {
		stp = 2;
		goto fail_1;
	}

	o_desc.wMaxPacketSize =
		cpu_to_le16(USB_DEBUG_MAX_PACKET_SIZE);

	dbg_desc.bDebugInEndpoint = i_desc.bEndpointAddress;
	dbg_desc.bDebugOutEndpoint = o_desc.bEndpointAddress;

#ifdef CONFIG_USB_G_DBGP_SERIAL
	dbgp.serial->in = dbgp.i_ep;
	dbgp.serial->out = dbgp.o_ep;

	dbgp.serial->in->desc = &i_desc;
	dbgp.serial->out->desc = &o_desc;
#endif

	return 0;

fail_1:
	dev_dbg(&dbgp.gadget->dev, "ep config: failure (%d)\n", stp);
	return -ENODEV;
}

static int dbgp_bind(struct usb_gadget *gadget,
		struct usb_gadget_driver *driver)
{
	int err, stp;

	dbgp.gadget = gadget;

	dbgp.req = usb_ep_alloc_request(gadget->ep0, GFP_KERNEL);
	if (!dbgp.req) {
		err = -ENOMEM;
		stp = 1;
		goto fail;
	}

	dbgp.req->buf = kmalloc(DBGP_REQ_EP0_LEN, GFP_KERNEL);
	if (!dbgp.req->buf) {
		err = -ENOMEM;
		stp = 2;
		goto fail;
	}

	dbgp.req->length = DBGP_REQ_EP0_LEN;

#ifdef CONFIG_USB_G_DBGP_SERIAL
	dbgp.serial = kzalloc(sizeof(struct gserial), GFP_KERNEL);
	if (!dbgp.serial) {
		stp = 3;
		err = -ENOMEM;
		goto fail;
	}

	if (gserial_alloc_line(&tty_line)) {
		stp = 4;
		err = -ENODEV;
		goto fail;
	}
#endif

	err = dbgp_configure_endpoints(gadget);
	if (err < 0) {
		stp = 5;
		goto fail;
	}

	dev_dbg(&dbgp.gadget->dev, "bind: success\n");
	return 0;

fail:
	dev_dbg(&gadget->dev, "bind: failure (%d:%d)\n", stp, err);
	dbgp_unbind(gadget);
	return err;
}

static void dbgp_setup_complete(struct usb_ep *ep,
				struct usb_request *req)
{
	dev_dbg(&dbgp.gadget->dev, "setup complete: %d, %d/%d\n",
		req->status, req->actual, req->length);
}


// TODO: Turn dbgp_setup_any into dbgp_setup_standard and dbgp_setup_class
static int dbgp_setup_any(struct usb_gadget *gadget,
	const struct usb_ctrlrequest *ctrl)
{
	int ret = -EOPNOTSUPP;
	struct usb_request *req = dbgp.req;
	u8 bRequest = ctrl->bRequest;
	u16 wIndex = le16_to_cpu(ctrl->wIndex);
	u16 wValue = le16_to_cpu(ctrl->wValue);
	u16 wLength = le16_to_cpu(ctrl->wLength);
	int err = -EOPNOTSUPP;


	switch (bRequest) {
	case USB_REQ_GET_DESCRIPTOR:
		if (ctrl->bRequestType != USB_DIR_IN)
			break;
		switch (wValue>>8) {
		case USB_DT_DEVICE:
			dev_dbg(&dbgp.gadget->dev, "setup: desc device\n");
			ret = sizeof device_desc;
			memcpy(req->buf, &device_desc, ret);
			device_desc.bMaxPacketSize0 = gadget->ep0->maxpacket;
			break;
		case USB_DT_DEBUG:
			dev_dbg(&dbgp.gadget->dev, "setup: desc debug\n");
			ret = sizeof dbg_desc;
			memcpy(req->buf, &dbg_desc, ret);
			break;
		case USB_DT_CONFIG:
			dev_dbg(&dbgp.gadget->dev, "get configuration descriptor *TODO*\n");
			ret = dbgp_build_config_buf(req->buf, gadget,
					wValue >> 8, wValue & 0xff,
					gadget_is_otg(gadget));
			if (ret >= 0)
				ret = min(wLength, (u16)ret);
			break;
		case USB_DT_STRING:
			dev_dbg(&dbgp.gadget->dev, "get string descriptor *TODO*\n");
			ret = usb_gadget_get_string(&dbgp_string_table,
					wValue & 0xff, req->buf);
			if (ret >= 0)
				ret = min(wLength, (u16)ret);
			break;
		default:
			pr_err("dbgp_setup_any: unknown GET_DESCRIPTOR request, type=%02x, "
				"request=%02x, value=%04x, index=%04x, length=%d\n",
				ctrl->bRequestType, bRequest,
				wValue, wIndex, wLength);
			goto fail_any;
		}
		err = 0;
		break;
	case USB_REQ_SET_FEATURE:
		if ( wValue == USB_DEVICE_DEBUG_MODE) {
		dev_dbg(&dbgp.gadget->dev, "setup: feat debug\n");
#ifdef CONFIG_USB_G_DBGP_PRINTK
		err = dbgp_enable_ep();
#else
		err = dbgp_configure_endpoints(gadget);
		if (err < 0) {
			goto fail_any;
		}
		err = gserial_connect(dbgp.serial, tty_line);
#endif
		dev_dbg(&dbgp.gadget->dev, "setup: feat debug (%d)\n", err);
		if (err < 0)
			goto fail_any;
		} else
			goto fail_any;
		break;
	case USB_REQ_SET_CONFIGURATION:
		dev_dbg(&dbgp.gadget->dev, "set configuration *TODO*\n");
		break;
	default:
		pr_err("dbgp_setup_any: unknown /any/ request, type=%02x, "
				"request=%02x, value=%04x, index=%04x, length=%d\n",
				ctrl->bRequestType, bRequest,
				wValue, wIndex, wLength);
		break;
		goto fail_any;
	}

	return ret;
// TODO: get rid of this
fail_any:
	dev_dbg(&dbgp.gadget->dev,
	"setup: failure req %x v %x\n", bRequest, wValue);
	return err;

}

static int dbgp_setup(struct usb_gadget *gadget,
		      const struct usb_ctrlrequest *ctrl)
{
	u16 ret = 0;
	struct usb_request *req = dbgp.req;
	u8 bRequest = ctrl->bRequest;
	u16 wIndex = le16_to_cpu(ctrl->wIndex);
	u16 wValue = le16_to_cpu(ctrl->wValue);
	u16 wLength = le16_to_cpu(ctrl->wLength);
	int err = -EOPNOTSUPP;

	req->complete = dbgp_setup_complete;

	if (wLength > DBGP_REQ_LEN) {
		if (ctrl->bRequestType & USB_DIR_IN) {
			/* Cast away the const, we are going to overwrite on purpose. */
			__le16 *temp = (__le16 *)&ctrl->wLength;

			*temp = cpu_to_le16(DBGP_REQ_LEN);
			wLength = DBGP_REQ_LEN;
		} else {
			return err;
		}
	}

	pr_debug("dbgp_setup: new request, type=%02x, request=%02x, "
		"value=%04x, index=%04x, length=%d\n",
		ctrl->bRequestType, bRequest,
		wValue, wIndex, wLength);

	// For now do this always as _any suggests.
	ret = dbgp_setup_any(gadget, ctrl);

	req->length = min(wLength, ret);
	req->zero = ret < req->length;
	req->complete = dbgp_setup_complete;
	return usb_ep_queue(gadget->ep0, req, GFP_ATOMIC);
}

static struct usb_gadget_driver dbgp_driver = {
	.function = "dbgp",
	.max_speed = USB_SPEED_HIGH,
	.bind = dbgp_bind,
	.unbind = dbgp_unbind,
	.setup = dbgp_setup,
	.reset = dbgp_disconnect,
	.disconnect = dbgp_disconnect,
	.driver	= {
		.owner = THIS_MODULE,
		.name = "dbgp"
	},
};

static int __init dbgp_init(void)
{
	return usb_gadget_probe_driver(&dbgp_driver);
}

static void __exit dbgp_exit(void)
{
	usb_gadget_unregister_driver(&dbgp_driver);
#ifdef CONFIG_USB_G_DBGP_SERIAL
	gserial_free_line(tty_line);
#endif
}

MODULE_AUTHOR("Stephane Duverger");
MODULE_LICENSE("GPL");
module_init(dbgp_init);
module_exit(dbgp_exit);
